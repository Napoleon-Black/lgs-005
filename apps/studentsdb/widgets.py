from django.forms import widgets

class TestWidget(widgets.Widget):
    def get_context(self, name, value, attrs=None):
        context = super().get_context(name, value, attrs)
        context['some_data'] = [1, 2, 3, 4, 5]
        return context

    template_name = 'studentsdb/widgets/test-widget.html'