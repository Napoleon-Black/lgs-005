from django.contrib import admin
from .models import Students

# Register your models here.
@admin.register(Students)
class StudentsAdmin(admin.ModelAdmin):
    # fields = ('name', 'status')
    exclude = ('status',)
    empty_value_display = 'Test@mail.com'
