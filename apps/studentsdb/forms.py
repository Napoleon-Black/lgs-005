from django import forms

from .widgets import TestWidget
from .models import Students


class UserForm(forms.Form):
    # date = forms.DateTimeField(label='Select Date', input_formats='%Y-%m-%d')
    name = forms.CharField(
        label='Your Name',
        max_length=50
    )
    password = forms.CharField(widget=forms.PasswordInput())
    # mail = forms.EmailField(label='Your E-mail')


class AddStudentForm(forms.ModelForm):
    class Meta:
        model = Students
        fields = ('name', 'email', 'status')