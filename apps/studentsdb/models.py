from django.db import models


# Create your models here.
class Students(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField(blank=True, null=True)
    status = models.BooleanField(default=False)

    def __str__(self):
    	return '{name} - {email}'.format(
    		name=self.name, email=self.email
    	)
