from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

from .models import Students
from .forms import UserForm
from .forms import AddStudentForm


@login_required
def students_page(request):
    students = Students.objects.all()
    return render(request, 'studentsdb/index.html', {'students': students})


def test_form(request):

    if request.method == 'POST':
        user_form = UserForm(request.POST)

        if user_form.is_valid():
            username = request.POST.get('name')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user:
                login(request, user)

    if request.method == 'GET':
        user_form = UserForm()

    return render(
        request, 'studentsdb/test-form.html', {'user_form': user_form}
    )


def test_students_form(request):

    if request.method == 'POST':
        students_form = AddStudentForm(request.POST)

        if students_form.is_valid():
            students_form.save()

    students_form = AddStudentForm()

    return render(
        request,
        'studentsdb/test-students-form.html',
        {'students_form': students_form}
    )
