from django.urls import path
from django.urls import re_path

from .views import students_page
from .views import test_form
from .views import test_students_form

urlpatterns = [
    path('', students_page, name='students'),
    path('test-form/', test_form, name='test_form'),
    path('test-students-form/', test_students_form, name='test_students')
]
